Zim Recently Modified Pages Plugin
==================================

This is a plugin for [Zim Desktop Wiki](http://www.zim-wiki.org/) that lists your recently modified pages
in descending order. Where the history bar at the top corresponds to your previously viewed pages, this
plugin records only pages you modify.

This plugin is able to base off of VCS information (working tree changes followed by commit history), which
is preferred, or filesystem modification time, which may be slow for large notebooks. Currently, only the git
VCS backend is implemented; feel free to fork and implement the others if you need them.

To install, clone this repo to ~/.local/share/zim/plugins/zim-recently-modified-pages, and then open Zim
and enable it through the plugins interface.

Thanks to Pavel_M, author of the Bookmarks Bar plugin, and of course Jaap Karssenberg, author of Zim
and the Table of Contents plugin.
