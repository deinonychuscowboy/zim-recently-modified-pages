#! /usr/bin/python

from gi.repository import GObject as gobject
from gi.repository import Gtk as gtk
from gi.repository import Pango as pango
import re
from os import walk,path

from zim.plugins.versioncontrol.git import GITApplicationBackend

from zim.plugins.versioncontrol import VCS

from zim.signals import ConnectorMixin

from zim.plugins import PluginClass
from zim.gui.mainwindow import MainWindowExtension
from zim.notebook import Path
from zim.gui.widgets import BrowserTreeView, LEFT_PANE, PANE_POSITIONS, WindowSidePaneWidget


class RecentlyModifiedPlugin(PluginClass):
	plugin_info={
		'name'       :_('Recently Modified Pages'),  # T: plugin name
		'description':_("""This plugin lists pages that have recently been modified
in the order of their modification, preferring version control
information if it is available.

Currently only supports git VCS backend.
"""),
		# T: plugin description
		'author'     :'DeinonychusCowboy <blackline.silverfield@gmail.com>',
	}

	plugin_preferences=(
		# key, type, label, default
		('pane','choice',_('Position in the window'),LEFT_PANE,PANE_POSITIONS),
		('path','bool',_('Display full path'),False),
		('prefer','bool',_('Use VCS data if available (may be faster, less accurate for many changes at once)'),True),
		('limit','int',_('Limit to this many entries (performance impact)'),50,(0,10000)),
		('commits','int',_('VCS: Check back only this many commits'),50,(0,1000)),
	)


class RMMainWindowExtension(MainWindowExtension):
	_location=None
	def __init__(self,plugin,window):
		super().__init__(plugin,window)
		self.widget=SidePaneRecentlyModified(self.window,plugin)
		self.on_preferences_changed(plugin.preferences)
		self.connectto(plugin.preferences,'changed',self.on_preferences_changed)

	def on_preferences_changed(self,preferences):
		if self.widget is None:
			return

		if preferences['pane'] != self._location:
			self._location=preferences['pane']
			try:
				self.window.remove(self.widget)
			except ValueError:
				pass
			self.window.add_tab(_('Recent'),self.widget,preferences['pane'])

		# T: widget label
		self.widget.show_all()

	def teardown(self):
		self.window.remove(self.widget)
		self.widget.disconnect_all()
		self.widget=None


TEXT_COL=0
PATH_COL=1


class RecentlyModifiedTreeView(BrowserTreeView):
	def __init__(self,ellipsis):
		super().__init__(RecentlyModifiedTreeModel())
		self.set_headers_visible(False)
		self.get_selection().set_mode(gtk.SelectionMode.SINGLE)

		cell_renderer=gtk.CellRendererText()
		if ellipsis:
			cell_renderer.set_property('ellipsize',pango.EllipsizeMode.END)
		column=gtk.TreeViewColumn('_heading_',cell_renderer,text=TEXT_COL)
		column.set_sizing(gtk.TreeViewColumnSizing.AUTOSIZE)
		# Without this sizing, column width only grows and never shrinks
		self.append_column(column)

	def clear(self):
		self.get_model().clear()

	def populate(self,entries):
		selected_iter=self.get_selection().get_selected()[1]
		if selected_iter is None:
			self.get_model().populate(entries)
		else:
			selection=self.get_model().get(selected_iter,0,1)
			self.get_model().populate(entries)
			iter=self.get_model().get_iter_first()

			while iter is not None:
				if self.get_model().get(iter,0,1)==selection:
					self.get_selection().select_iter(iter)
				iter=self.get_model().iter_next(iter)

class RecentlyModifiedTreeModel(gtk.TreeStore):
	def __init__(self):
		super().__init__(str,str)  # TEXT_COL, PATH_COL

	def populate(self,entries):
		set_a={}
		set_b={}
		i=0
		for b in entries:
			set_b[b]=i
			i+=1
		i=0
		for a in self:
			set_a[(self.get_value(a.iter,0),self.get_value(a.iter,1))]=i
			i+=1

		new_order={}
		removed=0

		for key in set_b:
			if key not in set_a:
				new_order[set_b[key]]=self.iter_n_children(None)
				self.append(None,key)
		for key in set_a:
			if key in set_b:
				if set_a[key]!=set_b[key]:
					new_order[set_b[key]]=set_a[key]
			else:
				new_order[self.iter_n_children(None)-1-removed]=set_a[key]
				removed+=1

		for key in range(0,len(new_order)):
			self.move_before(self.get_iter(str(new_order[key])),self.get_iter(str(key)))

		while removed > 0:
			self.remove(self.iter_nth_child(None,self.iter_n_children(None)-1))
			removed-=1

class RecentlyModifiedWidget(ConnectorMixin,gtk.ScrolledWindow):
	_window=None
	_repo=None
	_col=None
	_ellipsis=None
	_treeview=None
	_trim_name=None
	_limit=None
	_prefer=None
	_slash=None

	def __init__(self,window,ellipsis,plugin):
		super().__init__()
		self._col=re.compile(r"[^:]+")
		self._slash=re.compile(r"[/\\]")
		# TODO do we actually want this
		self._ellipsis=ellipsis
		self._treeview=RecentlyModifiedTreeView(self._ellipsis)
		self._treeview.connect('row-activated',self.on_heading_activated)
		self.add(self._treeview)
		self.connectto(plugin.preferences,'changed',self.on_preferences_changed)

		# XXX remove ui - use signals from pageview for this
		self._window=window
		self.connectto(window,'page-changed')
		self.connectto(window.notebook,'stored-page')

		self.on_preferences_changed(plugin.preferences)

	def on_preferences_changed(self,preferences):
		self._repo=Repo(self._window.notebook.folder, preferences)

		self._trim_name=not preferences['path']
		self._limit=preferences['limit']
		self._prefer=preferences['prefer']

		self._treeview.clear()
		self.load_list()

	def on_page_changed(self,ui,page):
		self.load_list()

	def on_stored_page(self,notebook,page):
		self.load_list()

	def load_list(self):
		vscroll_value=self.get_vadjustment().get_value()
		entries=self.build_list()
		self._treeview.populate(entries)
		gobject.idle_add(self.get_vadjustment().set_value,vscroll_value)

	def on_heading_activated(self,treeview,path,column):
		self.select_heading(path)

	def select_heading(self,path):
		"""
		Selects the given page.
		"""
		model=self._treeview.get_model()
		self._window.open_page(Path(model[path][PATH_COL]))

	def build_list(self):
		files=self.query_files()
		return self.get_titles(files)

	def get_titles(self,files):
		return ((self.get_title(file),file) for file in files)

	def get_title(self,file):
		return self._col.findall(file)[-1:][0] if self._trim_name else file

	def query_files(self):
		result=()
		if self._prefer:
			vcs_files=self._repo.log()
			vcs_working=self._repo.diff()

			seen={}
			data=[]
			for list in (vcs_working,vcs_files):
				for x in list:
					if x in seen:
						continue
					seen[x]=1
					data.append(x)
			result=tuple(self._slash.sub(":",x) for x in data[:self._limit])

		if len(result) == 0:
			# no repository results or user wants modification time
			data=[]
			for dirpath,dirnames,filenames in walk(self._window.notebook.folder.path):
				if dirpath[0]!=".":
					for filename in filenames:
						if filename[-4:]==".txt":
							data.append((path.getmtime(path.join(dirpath,filename)),path.relpath(path.join(dirpath,filename[:-4]), self._window.notebook.folder.path)))
			data.sort(key=lambda item: item[0],reverse=True)

			result=tuple(self._slash.sub(":",x[1]) for x in data[:self._limit])

		return result


class Repo:
	_repo=None
	_location=None
	_preferences=None

	def __init__(self,location,preferences):
		self._repo=VCS.detect_in_folder(location)
		self._location=location
		self._preferences=preferences

	def _convert(self,response):
		list=(y[:-4] for y in (x.strip() for x in response) if
			      y.endswith(".txt") and path.isfile(path.join(self._location.path,y)))

		return list

	def log(self):
		if self._repo is None:
			return ()
		elif type(self._repo) is GITApplicationBackend:
			response=self._repo.pipe(
				['log','--name-only','--date-order','--pretty=','-n'+str(self._preferences['commits'])])
			return self._convert(response)
		# TODO other types
		return ()

	def diff(self):
		if self._repo is None:
			return ()
		elif type(self._repo) is GITApplicationBackend:
			response=self._repo.pipe(
				['diff','HEAD', '--name-only'])
			return self._convert(response)
		# TODO other types
		return ()


class SidePaneRecentlyModified(RecentlyModifiedWidget,WindowSidePaneWidget):
	def __init__(self,window,preferences):
		super().__init__(window,True,preferences)
		self.title=_("Recent")
		self.set_policy(gtk.PolicyType.NEVER,gtk.PolicyType.AUTOMATIC)
		self.set_shadow_type(gtk.ShadowType.IN)
		self.set_size_request(-1,200)  # Fixed Height
